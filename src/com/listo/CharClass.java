package com.listo;

import com.listo.Util.Util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.listo.Stats.*;

public class CharClass {

    public static String selectClass() {
//        String[] classes= {"Barbarian", "Bard", "Cleric", "Druid", "Fighter (Eldritch Knight)", "Fighter (High Dexterity)",
//                "Fighter (High Strength)", "Monk", "Paladin", "Ranger", "Rogue (Thief)" , "Rogue (Assassin)",
//                "Rogue (Arcane Trickster)", "Sorcerer", "Warlock", "Wizard"};
        String[] classes= {"Fighter (Eldritch Knight)", "Fighter (High Dexterity)", "Fighter (High Strength)"};
        final String c= Util.pickOne(classes);
        System.out.println("++++++++++++++++++++++++"+c);
        return c;
    }


    public static void classAdjustments(Character c, List<Integer> raw) {
        switch (c.charClass) {
            case "Barbarian":
                c.preferredStats = new ArrayList<>(Arrays.asList(STRENGTH, CONSTITUTION, DEXTERITY));
                c.savingThrowProficiencies.put(STRENGTH, true);
                c.savingThrowProficiencies.put(CONSTITUTION, true);
                c.hd = 12;
                CharClassDetails.barbarian(c);
                break;
            case "Bard":
                c.preferredStats = new ArrayList<>(Arrays.asList(CHARISMA, DEXTERITY));
                c.savingThrowProficiencies.put(CHARISMA, true);
                c.savingThrowProficiencies.put(DEXTERITY, true);
                c.spellAbility=CHARISMA;
//                c.spellAbility=CHARISMA;
                c.hd = 8;
                CharClassDetails.bard(c);
                break;
            case "Cleric":
                c.preferredStats = new ArrayList<>(Arrays.asList(WISDOM, STRENGTH, CONSTITUTION));
                c.savingThrowProficiencies.put(WISDOM, true);
                c.savingThrowProficiencies.put(CHARISMA, true);
                c.spellAbility=WISDOM;
                c.hd = 8;
                CharClassDetails.cleric(c);
                break;
            case "Druid":
                c.preferredStats = new ArrayList<>(Arrays.asList(WISDOM, CONSTITUTION));
                c.savingThrowProficiencies.put(WISDOM, true);
                c.savingThrowProficiencies.put(INTELLIGENCE, true);

                c.spellAbility=WISDOM;
                c.hd = 8;
                CharClassDetails.druid(c);
                break;
            case "Fighter (Eldritch Knight)":
                c.preferredStats = new ArrayList<>(Arrays.asList(STRENGTH, INTELLIGENCE, DEXTERITY));
                c.savingThrowProficiencies.put(STRENGTH, true);
                c.savingThrowProficiencies.put(CONSTITUTION, true);
                c.spellAbility=INTELLIGENCE;;
                c.hd = 10;
                CharClassDetails.fighter(c);
                break;
            case "Fighter (High Dexterity)":
                c.preferredStats = new ArrayList<>(Arrays.asList(DEXTERITY, CONSTITUTION, STRENGTH));
                c.savingThrowProficiencies.put(STRENGTH, true);
                c.savingThrowProficiencies.put(CONSTITUTION, true);


                c.hd = 10;
                CharClassDetails.fighter(c);
                break;
            case "Fighter (High Strength)":
                c.preferredStats = new ArrayList<>(Arrays.asList(STRENGTH, CONSTITUTION, DEXTERITY));
                c.savingThrowProficiencies.put(STRENGTH, true);
                c.savingThrowProficiencies.put(CONSTITUTION, true);
                c.hd = 10;
                CharClassDetails.fighter(c);
                break;
            case "Monk":
                c.preferredStats = new ArrayList<>(Arrays.asList(DEXTERITY, WISDOM, CONSTITUTION));
                c.savingThrowProficiencies.put(STRENGTH, true);
                c.savingThrowProficiencies.put(DEXTERITY, true);
                c.spellAbility=WISDOM;
                c.hd = 8;
                CharClassDetails.monk(c);
                break;
            case "Paladin":
                c.preferredStats = new ArrayList<>(Arrays.asList(STRENGTH, CHARISMA, CONSTITUTION, WISDOM));
                c.savingThrowProficiencies.put(WISDOM, true);
                c.savingThrowProficiencies.put(CHARISMA, true);

                c.spellAbility=WISDOM;
                c.hd = 10;
                CharClassDetails.paladin(c);
                break;
            case "Ranger":
                c.preferredStats = new ArrayList<>(Arrays.asList(DEXTERITY, WISDOM, CONSTITUTION));
                c.savingThrowProficiencies.put(STRENGTH, true);
                c.savingThrowProficiencies.put(DEXTERITY, true);
                c.spellAbility=WISDOM;
                c.hd = 10;
                CharClassDetails.ranger(c);
                break;
            case "Rogue (Thief)":
                if (Roll.d(2) == 2) {
                    c.preferredStats = new ArrayList<>(Arrays.asList(DEXTERITY, CHARISMA, CONSTITUTION));

                } else {
                    c.preferredStats = new ArrayList<>(Arrays.asList(DEXTERITY, INTELLIGENCE, CONSTITUTION));
                }
                c.savingThrowProficiencies.put(DEXTERITY, true);
                c.savingThrowProficiencies.put(INTELLIGENCE, true);
                c.hd = 8;
//                rogue(charClass);
                break;
            case "Rogue (Assassin)":
                if (Roll.d(2) == 2) {
                    c.preferredStats = new ArrayList<>(Arrays.asList(DEXTERITY, CHARISMA, CONSTITUTION));

                } else {
                    c.preferredStats = new ArrayList<>(Arrays.asList(DEXTERITY, INTELLIGENCE, CONSTITUTION));
                }
                c.savingThrowProficiencies.put(DEXTERITY, true);
                c.savingThrowProficiencies.put(INTELLIGENCE, true);
                c.hd = 8;
//                rogue(charClass);
                break;
            case "Rogue (Arcane Trickster)":
                c.preferredStats = new ArrayList<>(Arrays.asList(DEXTERITY, INTELLIGENCE, CONSTITUTION));
                c.savingThrowProficiencies.put(DEXTERITY, true);
                c.savingThrowProficiencies.put(INTELLIGENCE, true);
                c.spellAbility=INTELLIGENCE;;
                c.hd = 8;
                //                druid(charClass);
                break;
            case "Sorcerer":
                c.preferredStats = new ArrayList<>(Arrays.asList(CHARISMA, CONSTITUTION));
                c.savingThrowProficiencies.put(CHARISMA, true);
                c.savingThrowProficiencies.put(CONSTITUTION, true);
                c.spellAbility=CHARISMA;
                c.hd = 6;
                CharClassDetails.sorcerer(c);
                break;
            case "Warlock":

                if (Roll.d(2)==2) {
                          c.preferredStats = new ArrayList<>(Arrays.asList(CHARISMA, DEXTERITY, CONSTITUTION));
                } else {
                      c.preferredStats = new ArrayList<>(Arrays.asList(CHARISMA, STRENGTH, CONSTITUTION));
                }
                c.savingThrowProficiencies.put(WISDOM, true);
                c.savingThrowProficiencies.put(CHARISMA, true);
                c.spellAbility=CHARISMA;
                c.hd = 8;
                CharClassDetails.warlock(c);
                break;
            case "Wizard":
                c.preferredStats = new ArrayList<>(Arrays.asList(INTELLIGENCE, DEXTERITY, CONSTITUTION));
                c.savingThrowProficiencies.put(INTELLIGENCE, true);
                c.savingThrowProficiencies.put(WISDOM, true);
                c.spellAbility=INTELLIGENCE;;
                c.hd = 6;
                CharClassDetails.wizard(c);
                break;
            default:
                System.out.println("error no class");
                break;
          }
//
        System.out.println(raw);

    }


}
