package com.listo;

import com.listo.Util.Const;
import com.listo.Util.Util;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.listo.Skills.*;

public class CharClassDetails {

    public static void barbarian(Character c) {
        List<Skills> choices = Arrays.asList(ANIMAL_HANDLING, ATHLETICS, INTIMIDATION, NATURE, PERCEPTION, SURVIVAL);
       Skills.addSkills(c, 2, choices);
        int x = 2;
        if (c.lvl > 2)
            x = 3;
        if (c.lvl > 5)
            x = 4;
        if (c.lvl > 11)
            x = 5;
        if (c.lvl > 16)
            x = 6;
        int y = 2;
        if (c.lvl > 8)
            y = 3;
        if (c.lvl > 15)
            y = 4;
        c.abil.add("Rage (" + x + " Rages, " + Util.showPlus(y) + " damage)");
        c.abil.add("Unarmored Defense");
        if (c.lvl >= 2) {
            c.abil.add("Reckless Attack");
            c.abil.add("Danger Sense");
        }
        if (c.lvl >= 3) {
            c.abil.add("Primal Path");
            x = Roll.d(2);
            if (x == 1) {
                c.archetype = "Path of the Berserker";
                c.abil.add("Path of the Berserker");
                c.abil.add("Frenzy");
            } else {
                c.archetype = "Path of the Totem Warrior - ";
                c.abil.add("Path of the Totem Warrior");
                c.abil.add("Spirit Seeker");
                String totem = "";
                x = Roll.d(3);
                if (x == 1) {
                    totem += "Totem Spirit (Bear)";
                    c.archetype += "Bear";

                } else if (x == 2) {
                    totem += "Totem Spirit (Eagle)";
                    c.archetype += "Eagle";

                } else if (x == 3) {
                    totem += "Totem Spirit (Wolf)";
                    c.archetype += "Wolf";
                }
                c.abil.add(totem);
            }
        }
        if (c.lvl >= 5) {
            c.abil.add("Extra Attack");
            c.abil.add("Fast Movement");
            c.move += 10;
        }
        if (c.lvl >= 6) {
            c.abil.add("Path Feature");
        }
        if (c.lvl >= 7) {
            c.abil.add("Feral Instinct");
        }
        if (c.lvl >= 9) {
            c.abil.add("Brutal Critical");
        }
        if (c.lvl >= 10) {
            c.abil.add("Spirit Walker");
        }


//        charClass.equipment.add("Explorer's pack");



    }


    public static void bard(Character c) {
        // 33% chance of a lute - first time we add music, so we shouldn't have a duplicate here
        if (Roll.d(3) == 1) {
            c.tools.add("Lute");
            c.equipment.add("Lute");
        } else {
            Character.addMusic(c, 1, true);
        }
        Character.addMusic(c, 2, false);
//        charClass.extraSkills += 3;
//
        List<Skills> choices = Arrays.asList(ACROBATICS,ANIMAL_HANDLING,ARCANA,ATHLETICS,DECEPTION,HISTORY,INSIGHT,INTIMIDATION,INVESTIGATION,MEDICINE,NATURE,PERCEPTION,PERFORMANCE,PERSUASION,RELIGION,SLEIGHT_OF_HAND,STEALTH,SURVIVAL);
        Skills.addSkills(c, 3, choices);
        String bardicInsp = "d6";
        if (c.lvl >= 5) {
            bardicInsp = "d8";
        }
        if (c.lvl >= 10) {
            bardicInsp = "d10";
        }
        c.abil.add("Spellcasting");
        c.abil.add("Bardic Inspiration (" + bardicInsp + ")");
        if (c.lvl > 1) {
            c.abil.add("Jack of all trades");
            String songRest = "d6";
            if (c.lvl > 8) {
                songRest="d8";
            }
            c.abil.add("Song of Rest ("+songRest+")");
        }
        if (c.lvl > 2) {
            c.abil.add("Expertise");
            int x = Roll.d(2);
            if (x == 2) {
                c.archetype="College of Lore";
                c.abil.add("Bard College (Lore)");
                c.abil.add("Bonus proficiencies");
                c.abil.add("Cutting words");
                if (c.lvl > 5) {
                    c.abil.add("Additional Magical Secrets");
                }
            } else {
                c.archetype= "College of Valor";
                c.abil.add("Bard College (Valor)");
                c.abil.add("Bonus proficiencies (arms)");
                c.abil.add("Combat inspiration");
                if (c.lvl > 5) {
                    c.abil.add("Extra Attack");
                }
            }

        }
        if (c.lvl > 4) {
            c.abil.add("Font of Inspiration");
        }
        if (c.lvl > 5) {
            c.abil.add("Countercharm");
        }
        if (c.lvl > 9) {
            c.abil.add("Magical Secrets");
        }
    }

    public static void cleric(Character c) {
        List<Skills> choices = Arrays.asList(HISTORY, INSIGHT, MEDICINE, PERSUASION, RELIGION);
       Skills.addSkills(c,2, choices);
        c.abil.add("Spellcasting");
        c.abil.add("Divine Domain");
        String clDomain="";
        if (c.archetype.length() == 0) {
            String[] subdomains= {"Knowledge", "Light", "Life", "Trickery", "War", "Tempest", "Nature", "Death"};
            c.archetype = Util.pickOne(subdomains);
        }

        switch (c.archetype) {
            case "Knowledge":
                c.abil.add("Blessings of Knowledge");
                c.extraLanguages += 2;
                choices = Arrays.asList(ARCANA, NATURE, HISTORY, RELIGION);
               Skills.addSkills(c, 2, choices);
                if (c.lvl > 1) {
                    c.abil.add("Channel Divinity: Turn Undead");
                    c.abil.add("Channel Divinity: Knowledge of the Ages");
                    c.abil.add("");

                }
                if (c.lvl > 5) {
                    c.abil.add("Channel Divinity: Read Thoughts");
                }
                if (c.lvl > 7) {
                    c.abil.add("Potent Spellcasting");
                }
                break;
            case "Life":
                c.abil.add("Bonus proficiency");
                c.abil.add("Discipline of Life");

                if (c.lvl > 1) {
                    c.abil.add("Channel Divinity: Turn Undead");
                    c.abil.add("Channel Divinity: Preserve Life (heals " + (5 * c.lvl) + " hp)");
                }

                if (c.lvl > 5) {
                    c.abil.add("Blessed Healer");
                }
                if (c.lvl > 7) {
                    c.abil.add("Divine Strike +1d8 Radiant dmg");
                }
                break;
            case "Light":
                c.abil.add("Bonus proficiency");
                c.abil.add("Warding Flare");



//            TODO    myCantrips.unshift("Light");




                if (c.lvl > 1) {
                    c.abil.add("Channel Divinity: Turn Undead");
                    c.abil.add("Channel Divinity: Radiance of the Dawn");
                }
                if (c.lvl > 5) {
                    c.abil.add("Improved Flare");
                }
                if (c.lvl > 7) {
                    c.abil.add("Potent Spellcasting");
                }
                break;
            case "Nature":
                c.abil.add("Acolyte of Nature");
                c.abil.add("Bonus proficiency");
                if (c.lvl > 1) {

                    c.abil.add("Channel Divinity: Turn Undead");
                    c.abil.add("Channel Divinity: Charm Animals and Plants");
                }
                if (c.lvl > 5) {
                    c.abil.add("Dampen Elements");
                }
                if (c.lvl > 7) {
                    c.abil.add("Divine Strike - 1d8 Cold, Fire, or Lighting dmg");
                }
                break;
            case  "Tempest":
                c.abil.add("Bonus proficiency");
                c.abil.add("Wrath of the Storm");

                if (c.lvl > 1) {
                    c.abil.add("Channel Divinity: Turn Undead");
                    c.abil.add("Channel Divinity: Destructive Wrath");
                }
                if (c.lvl > 5) {
                    c.abil.add("Thunderbolt Strike");
                }
                if (c.lvl > 7) {
                    c.abil.add("Divine Strike - 1d8 Thunder dmg");
                }
                break;
            case "Trickery":
                c.abil.add("Blessing of the Trickster");
                if (c.lvl > 1) {
                    c.abil.add("Channel Divinity: Turn Undead");
                    c.abil.add("Channel Divinity: Invoke Duplicity");

                }

                if (c.lvl > 5) {
                    c.abil.add("Channel Divinity: Cloak of Shadows");
                }
                if (c.lvl > 7) {
                    c.abil.add("Divine Strike - 1d8 Poison dmg");
                }
                break;
            case "War":
                c.abil.add("War Priest");
                c.abil.add("Bonus proficiency");
                if (c.lvl > 1) {
                    c.abil.add("Channel Divinity: Turn Undead");
                    c.abil.add("Channel Divinity: Guided Strike");
                }

                if (c.lvl > 5) {
                    c.abil.add("Channel Divinity: War God's Blessing");
                }
                if (c.lvl > 7) {
                    c.abil.add("Divine Strike (1d8 Extra Weapon dmg)");
                }
                break;
            case "Death":
                c.abil.add("Reaper");
                c.abil.add("Bonus proficiency");
                if (c.lvl > 1) {
                    c.abil.add("Channel Divinity: Turn Undead");
                    int x = 5 + (c.lvl * 2);
                    c.abil.add("Channel Divinity: Touch of Death " + Util.showPlus(x) + " necrotic damage");
                }
                if (c.lvl > 5) {
                    c.abil.add("Inescapable Destruction");
                }
                if (c.lvl > 7) {
                    c.abil.add("Divine Strike +1d8 necrotic damage");
                }
                break;
        }
        c.archetype += " domain";
        if (c.lvl > 7) {
            c.abil.add("Destroy Undead (CR 1)");
        } else if (c.lvl > 4) {
            c.abil.add("Destroy Undead (CR 1/2)");
        }
        if (c.lvl > 9) {
            c.abil.add("Divine Intervention");
        }

    }




    public static void druid(Character c) {
        List<Skills> choices = Arrays.asList(ANIMAL_HANDLING, ARCANA, INSIGHT, NATURE, MEDICINE, PERCEPTION, RELIGION, SURVIVAL);
       Skills.addSkills(c, 2,choices);
//        charClass.abil.add("");
//        charClass.abil.add("");
//        charClass.abil.add("");
//        charClass.abil.add("");
//        charClass.abil.add("");
//        charClass.abil.add("");
//        charClass.abil.add("");
//        charClass.abil.add("");
//        charClass.abil.add("");
        c.tools.add("Herbalism kit");
        c.lang.add("Druidic");
        c.abil.add("Spellcasting");
        if (c.lvl > 1) {
            //choose cirlce of moon or circle of land.
            int x = Roll.d(2);
            // circle of land 1st
            if (x==1) {
                //show 2nd levl abilities to wild shape
                if (c.lvl < 4) {
                    c.abil.add("Wild Shape (CR 1/4 creature - No flying or swimming speed)");
                } else if (c.lvl < 8) {
                    c.abil.add("Wild Shape (CR 1/2 creature - No flying speed)");
                } else if (c.lvl >7) {
                    c.abil.add("Wild Shape (CR 1 creature)");
                }
                circleOfLandSpells(c);
//                charClass.archetype += " (Circle of the Land  &mdash; "+land+")";
//                charClass.abil.add("Druid Circle (" + land + ")"
                c.abil.add("Bonus Cantrip");
                c.abil.add("Natural Recovery");
                if (c.lvl > 5)
                    c.abil.add("Land's Stride");
                if (c.lvl > 9)
                    c.abil.add("Nature's Ward");
            } else {
                // circle of the moon
                c.archetype = "Circle of the Moon";
                c.abil.add("Combat Wild Shape");
                String cr = "1";
                if (c.lvl > 5)
                    cr = "2";
                if (c.lvl > 8)
                    cr = "3";
                String details = " - No flying or swimming speed";
                if (c.lvl >3) {
                    details = " - No flying speed)";
                }
                if (c.lvl > 7) {
                    details = "";
                }
                c.abil.add("Circle Forms (CR " + cr + details + ")");
                if (c.lvl > 5) {
                    c.abil.add("Primal Strike");
                }
                if (c.lvl > 9) {
                    c.abil.add("Elemental Wild Shape");
                }
            }
        }

    }


    public static void fighter(Character c) {
//        List<Skills> choices = Arrays.asList(ACROBATICS, ANIMAL_HANDLING, ATHLETICS, HISTORY, INSIGHT, INTIMIDATION, PERCEPTION, SURVIVAL);
        List<Skills> choices = Arrays.asList(ACROBATICS, ANIMAL_HANDLING, ATHLETICS, HISTORY, INSIGHT, INTIMIDATION, PERCEPTION, SURVIVAL);
//       Skills.addSkills(c, 2,choices);
        Skills.addSkills(c, 2, choices);
        boolean ek = false;
        boolean ch = false;
        boolean ba = false;
        //eldritfch knight learns any
        String[] styles=new String[]{"Archery", "Defense", "Dueling", "Great Weapon Fighting", "Protection", "Two-Weapon Fighting"};
        if (c.archetype.contains("Eldritch")) {
            ek=true;
        } else {
            //figure out your archetype unless we know you're eldritch
            if (Roll.d(2) == 2) {
                ch=true;
            } else {
                ba=true;
            }
        }
        if (c.archetype.contains("Dex")) {
            styles=new String[]{"Archery","Defense", "Dueling", "Protection", "Two-Weapon Fighting"};
        }
        if (c.archetype.contains("Strength")) {
            styles=new String[]{"Defense", "Dueling", "Great Weapon Fighting", "Protection", "Two-Weapon Fighting"};

        }
        String style = Util.pickOne(styles);
        c.abil.add("Fighting Style ("+style+")");
        c.abil.add("Second Wind");
        if (c.lvl > 1)
            c.abil.add("Action Surge");
        if ((c.lvl > 2) && ek) {
            c.abil.add("Martial Archetype (Eldritch Knight)");
            c.abil.add("Spellcasting");
            c.abil.add("Weapon Bond");
            c.archetype="Eldritch Knight";
        } else if ((c.lvl > 2) && ba) {
            Character.addTools(c, 1,false);
            int supDie=8;
            if (c.lvl > 9) {
                supDie=10;
            }
            int numberDice=4;
            if (c.lvl > 6) {
                numberDice=5;
            }
            c.abil.add("Martial Archetype (Battle Master)");
            c.abil.add(numberDice+" Superiority Dice (d"+supDie+")");
            addManeuvers(c);
            c.archetype="Battle Master";
        } else if ((c.lvl > 2) && ch) {
            c.abil.add("Martial Archetype (Champion)");
            c.abil.add("Improved Critical");
            c.archetype="Champion";
        }
        if (c.lvl >= 5) {
            c.abil.add("Extra Attack");
        }
        if (c.lvl >= 7 && ek) {
            c.abil.add("War Magic");
        } else if ((c.lvl >= 7) && ba) {
            c.abil.add("Know Your Enemy");
        } else if ((c.lvl >= 7) && ch) {
            c.abil.add("Remarkable Athlete");
        }
        if (c.lvl > 8) {
            c.abil.add("Indomitable (one use)");
        }
        if ((c.lvl >= 10) && ek) {
            c.abil.add("Eldritch Strike");
        } else if ((c.lvl >= 10) && ba) {
            c.abil.add("Improved Combat Superiority");
        } else if ((c.lvl >= 10) && ch) {
//            style=Util.pickOne(styles);
//            while (!charClass.abil.contains(style)) {
//                style=Util.pickOne(styles);
//            }
            //TODO
//            charClass.abil.add("Additional Fighting Style ("+ style+ ")");



        }
    }

    private static void addManeuvers(Character c) {
        int maneuvers=3;
        if (c.lvl > 6)
            maneuvers+=2;
        if (c.lvl > 9)
            maneuvers+=2;
        List<String> answer=Util.pickMultiple(Const.maneuvers, maneuvers);
        Collections.sort(answer);
        String maneuverText = Util.displayList(answer);
        c.abil.add(maneuvers+" maneuvers ("+maneuverText+")");
    }

    public static void monk(Character c) {
        List<Skills> choices = Arrays.asList(ACROBATICS, ATHLETICS, HISTORY, INSIGHT, RELIGION, STEALTH);
       Skills.addSkills(c, 2,choices);

        //        charClass.abil.add("");
//        charClass.abil.add("");
//        charClass.abil.add("");
//        charClass.abil.add("");
    }


    public static void paladin(Character c) {
        List<Skills> choices = Arrays.asList(INTIMIDATION, ATHLETICS, MEDICINE, INSIGHT, RELIGION, PERSUASION);
       Skills.addSkills(c, 2,choices);

        //        charClass.abil.add("");
//        charClass.abil.add("");
//        charClass.abil.add("");
//        charClass.abil.add("");
    }

    public static void ranger(Character c) {
        List<Skills> choices = Arrays.asList(ANIMAL_HANDLING, ATHLETICS, INSIGHT, INTIMIDATION, NATURE, PERCEPTION, STEALTH, SURVIVAL);
        Skills.addSkills(c,3,choices);

        //        charClass.abil.add("");
//        charClass.abil.add("");
//        charClass.abil.add("");
//        charClass.abil.add("");
    }

    public static void rogue(Character c) {
        List<Skills> choices = Arrays.asList(ACROBATICS, ATHLETICS, DECEPTION, INSIGHT, INTIMIDATION, INVESTIGATION, PERCEPTION, PERFORMANCE, PERSUASION, SLEIGHT_OF_HAND, STEALTH);
       Skills.addSkills(c, 4,choices);

        //        charClass.abil.add("");
//        charClass.abil.add("");
//        charClass.abil.add("");
//        charClass.abil.add("");
    }

    public static void sorcerer(Character c) {
        List<Skills> choices = Arrays.asList(ARCANA, PERSUASION, DECEPTION, INSIGHT, INTIMIDATION, RELIGION);
       Skills.addSkills(c, 2,choices);

        //        charClass.abil.add("");
//        charClass.abil.add("");
//        charClass.abil.add("");
//        charClass.abil.add("");
    }

    public static void warlock(Character c) {
        List<Skills> choices = Arrays.asList(ARCANA, NATURE, HISTORY, INVESTIGATION, DECEPTION, INTIMIDATION, RELIGION);
       Skills.addSkills(c, 2,choices);

        //        charClass.abil.add("");
//        charClass.abil.add("");
//        charClass.abil.add("");
//        charClass.abil.add("");

    }

    public static void wizard(Character c) {
        List<Skills> choices = Arrays.asList(ARCANA, HISTORY, INVESTIGATION, INSIGHT, MEDICINE, RELIGION);
       Skills.addSkills(c, 2,choices);

        //        charClass.abil.add("");
//        charClass.abil.add("");
//        charClass.abil.add("");
//        charClass.abil.add("");

    }


//
//    public static void (Character charClass, Skills sk) {
//
//    }
//
//    public static void (Character charClass, Skills sk) {
//
//    }
//
//    public static void (Character charClass, Skills sk) {
//
//    }
//
//    public static void (Character charClass, Skills sk) {
//
//    }
//
//    public static void (Character charClass, Skills sk) {
//
//    }
//
//


    private static void circleOfLandSpells(Character c) {
//        drCircle = 0;
//                x=Math.floor(Math.random() * 8);
//                //set circle of land to user's choice
//                if (xx > 0)
//                    x = xx-1;
//                if (x==0) {
//                    land += "Arctic";
//                    if (charClass.lvl >2) {
//                        ciSpells.push("Hold Person");
//                        ciSpells.push("Spike Growth");
//                    }
//                    if (charClass.lvl > 4) {
//                        ciSpells.push("Sleet Storm");
//                        ciSpells.push("Slow");
//                    }
//                    if (charClass.lvl > 6) {
//                        ciSpells.push("Freedom of Movement");
//                        ciSpells.push("Ice Storm");
//                    }
//                    if (charClass.lvl > 8) {
//                        ciSpells.push("Commune with Nature");
//                        ciSpells.push("Cone of Cold");
//                    }
//                }
//                if (x==1) {
//                    land += "Coast";
//                    if (charClass.lvl >2) {
//                        ciSpells.push("Mirror Image");
//                        ciSpells.push("Misty Step");
//                    }
//                    if (charClass.lvl > 4) {
//                        ciSpells.push("Water Breathing");
//                        ciSpells.push("Water Walk");
//                    }
//                    if (charClass.lvl > 6) {
//                        ciSpells.push("Control Water");
//                        ciSpells.push("Freedom of Movement");
//                    }
//                    if (charClass.lvl > 8) {
//                        ciSpells.push("Conjure Elemental");
//                        ciSpells.push("Scrying");
//                    }
//                }
//                if (x==2) {
//                    land += "Desert";
//                    if (charClass.lvl >2) {
//                        ciSpells.push("Blur");
//                        ciSpells.push("Silence");
//                    }
//                    if (charClass.lvl > 4) {
//                        ciSpells.push("Create Food and Water");
//                        ciSpells.push("Protection from Energy");
//                    }
//                    if (charClass.lvl > 6) {
//                        ciSpells.push("Blight");
//                        ciSpells.push("Hallucinatory Terrain");
//                    }
//                    if (charClass.lvl > 8) {
//                        ciSpells.push("Insect Plague");
//                        ciSpells.push("Wall of Stone");
//                    }
//                }
//                if (x==3) {
//                    land += "Forest";
//                    if (charClass.lvl >2) {
//                        ciSpells.push("Barkskin");
//                        ciSpells.push("Spider Climb");
//
//                    }
//                    if (charClass.lvl > 4) {
//                        ciSpells.push("Call Lightning");
//                        ciSpells.push("Plant Growth");
//                    }
//                    if (charClass.lvl > 6) {
//                        ciSpells.push("Divination");
//                        ciSpells.push("Freedom of Movement");
//                    }
//                    if (charClass.lvl > 8) {
//                        ciSpells.push("Commune with Nature");
//                        ciSpells.push("Tree Stride");
//                    }
//                }
//                if (x==4) {
//                    land += "Grassland";
//                    if (charClass.lvl >2) {
//                        ciSpells.push("Invisibility");
//                        ciSpells.push("Pass without Trace");
//
//                    }
//                    if (charClass.lvl > 4) {
//                        ciSpells.push("Daylight");
//                        ciSpells.push("Haste");
//                    }
//                    if (charClass.lvl > 6) {
//                        ciSpells.push("Divination");
//                        ciSpells.push("Freedom of Movement");
//                    }
//                    if (charClass.lvl > 8) {
//                        ciSpells.push("Dream");
//                        ciSpells.push("Insect Plague");
//                    }
//                }
//                if (x==5) {
//                    land += "Mountain";
//                    if (charClass.lvl >2) {
//                        ciSpells.push("Spider Climb");
//                        ciSpells.push("Spike Growth");
//
//                    }
//                    if (charClass.lvl > 4) {
//                        ciSpells.push("Lightning Bolt");
//                        ciSpells.push("Meld into Stone");
//                    }
//                    if (charClass.lvl > 6) {
//                        ciSpells.push("Stone Shape");
//                        ciSpells.push("Stoneskin");
//                    }
//                    if (charClass.lvl > 8) {
//                        ciSpells.push("Passwall");
//                        ciSpells.push("Wall of Stone");
//                    }
//                }
//                if (x==6) {
//                    land += "Swamp";
//                    if (charClass.lvl >2) {
//                        ciSpells.push("Darkness");
//                        ciSpells.push("Melf's Acid Arrow");
//
//                    }
//                    if (charClass.lvl > 4) {
//                        ciSpells.push("Water Walk");
//                        ciSpells.push("Stinking Cloud");
//                    }
//                    if (charClass.lvl > 6) {
//                        ciSpells.push("Freedom of Movement");
//                        ciSpells.push("Locate Creature");
//                    }
//                    if (charClass.lvl > 8) {
//                        ciSpells.push("Insect Plague");
//                        ciSpells.push("Scrying");
//                    }
//                }
//                if (x==7) {
//                    land += "Underdark";
//                    if (charClass.lvl >2) {
//                        ciSpells.push("Spider Climb");
//                        ciSpells.push("Web");
//
//                    }
//                    if (charClass.lvl > 4) {
//                        ciSpells.push("Gaseous Form");
//                        ciSpells.push("Stinking Cloud");
//                    }
//                    if (charClass.lvl > 6) {
//                        ciSpells.push("Greater Invisibility");
//                        ciSpells.push("Stone Shape");
//                    }
//                    if (charClass.lvl > 8) {
//                        ciSpells.push("Cloudkill");
//                        ciSpells.push("Insect Plague");
//                    }
//                }
//
    }


}
