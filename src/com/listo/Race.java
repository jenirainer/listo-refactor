package com.listo;

import java.util.HashMap;
import java.util.Map;

import static com.listo.Skills.*;
import static com.listo.Stats.*;

public enum Race {
    AARAKOCRA, DRAGONBORN, HILL_DWARF, MOUNTAIN_DWARF, DROW_ELF, HIGH_ELF,
    WOOD_ELF, AIR_GENASI, EARTH_GENASI, FIRE_GENASI, WATER_GENASI, FOREST_GNOME,
    ROCK_GNOME, SVIRFNEBLIN_GNOME, GOLIATH, HALF_ELF, HALF_ORC, LIGHTFOOT_HALFLING,
    STOUT_HALFLING, HUMAN, TIEFLING;

    public static Race selectRace() {
        Race[] races;
        if (Roll.d(4) == 4) {
            races= new Race[]{
                    AARAKOCRA, DRAGONBORN,  DROW_ELF, AIR_GENASI, EARTH_GENASI, FIRE_GENASI, WATER_GENASI,
                    FOREST_GNOME, ROCK_GNOME, SVIRFNEBLIN_GNOME, GOLIATH, HALF_ELF, HALF_ORC, TIEFLING};
        } else {
            races = new Race[]{
                    HILL_DWARF, MOUNTAIN_DWARF, HIGH_ELF, WOOD_ELF, LIGHTFOOT_HALFLING,
                    STOUT_HALFLING, HUMAN};
        }
        int roll = Roll.d(races.length)-1;
        return races[roll];
//        return Util.randomEnum(races);
    }

    public static void raceAdjustments(Character c) {
        c.lang.add("Common");
        switch (c.race) {
            case DRAGONBORN:
                c.stats.put(STRENGTH, 2);
                c.stats.put(CHARISMA, 1);
                c.lang.add("Draconic");
                c.move = 30;
                addDragonbornAbilities(c);
                break;
            case FOREST_GNOME:
                c.stats.put(INTELLIGENCE, 2);
                c.stats.put(DEXTERITY, 1);
                c.lang.add("Gnomish");
                c.move = 25;
                c.size = "Small";
                c.abil.add("Darkvision");
                c.abil.add("Gnome Cunning");
                c.abil.add("Natural Illusionist");
                c.abil.add("Speak with Small Animals");
                c.raceSpells.add("Minor Illusion");
                c.raceSpellAbility = INTELLIGENCE;
                break;
            case HALF_ELF:
                c.stats.put(CHARISMA, 2);
                c.extraSkills = 2;
                // twp other abilities increase by 1
                c.stats = halfElfIncreaseTwoStats(c);
                c.lang.add("Elvish");
                c.move = 30;
                c.extraLanguages = 1;
//                System.out.println(sk);
//                Skills.printSkills(charClass, sk);
                c.abil.add("Darkvision");
                c.abil.add("Fey Ancestry");
                c.abil.add("2 skills");
                break;
            case HALF_ORC:
                c.stats.put(STRENGTH, 2);
                c.stats.put(CONSTITUTION, 1);
                c.move = 30;
                c.lang.add("Orc");
                c.abil.add("Darkvision");
                c.abil.add("Menacing (Intimidate skill)");
                c.skills.put(INTIMIDATION, 1);
//                Skills.giveProf(sk.inti);
                c.abil.add("Relentless Endurance");
                c.abil.add("Savage Attack");
                break;
            case TIEFLING:
                c.stats.put(CHARISMA, 2);
                c.stats.put(INTELLIGENCE, 1);
                c.move = 30;
                c.lang.add("Infernal");
                c.abil.add("Darkvision");
                c.abil.add("Hellish Resistance");
                c.abil.add("Infernal Legacy");
                c.raceSpellAbility = CHARISMA;
                c.raceSpells.add("Thaumaturgy");
                if (c.lvl > 2)
                    c.raceSpells.add("Hellish Rebuke (once a day)");
                if (c.lvl > 4)
                    c.raceSpells.add("Darkness (once a day)");
//                if (gender==2) {
//                    name = tieflingNameF[Math.floor(Math.random() * tieflingNameF.length)];
//                } else {
//                    name = tieflingNameM[Math.floor(Math.random() * tieflingNameM.length)];
//                }
//                name += " " + tieflingNameLast[Math.floor(Math.random() * tieflingNameLast.length)];
                break;

            case DROW_ELF:
                c.stats.put(DEXTERITY, 2);
                c.stats.put(CHARISMA, 1);
                c.skills.put(PERCEPTION, 1);
//                sk.perc.value = 'y';
                c.raceSpellAbility = CHARISMA;
                c.move = 30;
                c.lang.add("Elvish");
                c.abil.add("Superior Darkvision (120 ft)");
                c.abil.add("Sunlight sensitivity");
                c.abil.add("Drow Magic");
                c.abil.add("Keen Senses");
                c.abil.add("Fey Ancestry");
                c.abil.add("Trance");
                c.raceSpells.add("Dancing Lights");
                if (c.lvl > 2)
                    c.raceSpells.add("Faerie Fire (once a day)");
                if (c.lvl > 4)
                    c.raceSpells.add("Darkness (once a day) ");
//                if (gender==2) {
//                    name = elfNameF[Math.floor(Math.random() * elfNameF.length)];
//                } else {
//                    name = elfNameM[Math.floor(Math.random() * elfNameM.length)];
//                }
//                name += " " + elfNameLast[Math.floor(Math.random() * elfNameLast.length)];
                break;
            case ROCK_GNOME:
                c.stats.put(INTELLIGENCE, 2);
                c.stats.put(CONSTITUTION, 1);
                c.move = 25;
                c.size = "Small";
                c.lang.add("Gnomish");
//                if (gender==2) {
//                    name = gnomeNameF[Math.floor(Math.random() * gnomeNameF.length)];
//                } else {
//                    name = gnomeNameM[Math.floor(Math.random() * gnomeNameM.length)];
//                }
//                name += " " + gnomeNameLast[Math.floor(Math.random() * gnomeNameLast.length)];
                c.abil.add("Darkvision");
                c.abil.add("Gnome Cunning");
                c.abil.add("Artificer's Lore");
                c.tools.add("Tinker's tools");
                break;
            case AARAKOCRA:
                c.stats.put(DEXTERITY, 2);
                c.stats.put(WISDOM, 1);
                c.move = 25;
                c.size = "Medium";
                c.lang.add("Aarakocra");
                c.lang.add("Auran");
                c.abil.add("Flight");
                c.abil.add("Talons");
//                name = aarName[Math.floor(Math.random() * aarName.length)];
                break;
            case SVIRFNEBLIN_GNOME:
                c.stats.put(INTELLIGENCE, 2);
                c.stats.put(DEXTERITY, 1);
                c.move = 25;
                c.size = "Small";
                c.lang.add("Gnomish");
                c.lang.add("Undercommon");
//                if (gender==2) {
//                    name = deepGnomeNameF[Math.floor(Math.random() * deepGnomeNameF.length)];
//                } else {
//                    name = deepGnomeNameM[Math.floor(Math.random() * deepGnomeNameM.length)];
//                }
//                name += " " + deepGnomeNameLast[Math.floor(Math.random() * deepGnomeNameLast.length)];
                c.abil.add("Superior Darkvision (120 ft)");
                c.abil.add("Gnome Cunning");
                c.abil.add("Stone Camouflage");
                break;
            case AIR_GENASI:
                c.stats.put(CONSTITUTION, 2);
                c.stats.put(DEXTERITY, 1);
                c.abil.add("Unending Breath");
                c.abil.add("Mingle with the Wind");
                c.raceSpells.add("Levitate (once per long rest)");
                c.raceSpellAbility = CONSTITUTION;
                c.move = 30;
                c.size = "Medium";
                c.lang.add("Primordial");
//                if (gender==2) {
//                    name = humanNameF[Math.floor(Math.random() * humanNameF.length)];
//                } else {
//                    name = humanNameM[Math.floor(Math.random() * humanNameM.length)];
//                }
//                name += " " + humanNameLast[Math.floor(Math.random() * humanNameLast.length)];
                break;
            case EARTH_GENASI:
                c.stats.put(CONSTITUTION, 2);
                c.stats.put(STRENGTH, 1);
                c.abil.add("Earth Walk");
                c.abil.add("Merge with Stone");

                c.raceSpells.add("Pass Without Trace (once per long rest)");
                c.raceSpellAbility = CONSTITUTION;

                c.move = 30;
                c.size = "Medium";
                c.lang.add("Primordial");
//                if (gender==2) {
//                    name = humanNameF[Math.floor(Math.random() * humanNameF.length)];
//                } else {
//                    name = humanNameM[Math.floor(Math.random() * humanNameM.length)];
//                }
//                name += " " + humanNameLast[Math.floor(Math.random() * humanNameLast.length)];
                break;
            case FIRE_GENASI:
                c.stats.put(CONSTITUTION, 2);
                c.stats.put(INTELLIGENCE, 1);

                c.abil.add("Darkvision");
                c.abil.add("Fire Resistance");
                c.abil.add("Reach to the Blaze");
                c.raceSpells.add("Produce Flame");
                if (c.lvl > 2)
                    c.raceSpells.add("Burning Hands (as a 1st level spell once per long rest)");
                c.raceSpellAbility = CONSTITUTION;

                c.move = 30;
                c.size = "Medium";
                c.lang.add("Primordial");
//                if (gender==2) {
//                    name = humanNameF[Math.floor(Math.random() * humanNameF.length)];
//                } else {
//                    name = humanNameM[Math.floor(Math.random() * humanNameM.length)];
//                }
//                name += " " + humanNameLast[Math.floor(Math.random() * humanNameLast.length)];
                break;
            case WATER_GENASI:
                c.stats.put(CONSTITUTION, 2);
                c.stats.put(WISDOM, 1);

                c.abil.add("Acid Resistance");
                c.abil.add("Amphibious");
                c.abil.add("Swim");
                c.abil.add("Call to the Wave");
                c.raceSpells.add("Shape Water");
                if (c.lvl > 2)
                    c.raceSpells.add("Create or Destroy Water (as a 2nd level spell once per long rest)");
                c.raceSpellAbility = CONSTITUTION;

                c.move = 30;
                c.size = "Medium";
                c.lang.add("Primordial");
//                if (gender==2) {
//                    name = humanNameF[Math.floor(Math.random() * humanNameF.length)];
//                } else {
//                    name = humanNameM[Math.floor(Math.random() * humanNameM.length)];
//                }
//                name += " " + humanNameLast[Math.floor(Math.random() * humanNameLast.length)];
                break;
            case GOLIATH:
                c.stats.put(STRENGTH, 2);
                c.stats.put(CONSTITUTION, 1);

                c.abil.add("Natural Athlete (Athletics skill)");
                c.abil.add("Stone's Endurance");
                c.abil.add("Powerful Build");
                c.abil.add("Mountain Born");
                c.skills.put(ATHLETICS, 1);
//                Skills.giveProf(sk.athl);
                c.move = 30;
                c.size = "Medium";
                c.lang.add("Giant");
//                name = goliathNameBirth[Math.floor(Math.random() * goliathNameBirth.length)] + "  \"" + goliathNameNick[Math.floor(Math.random() * goliathNameNick.length)] + "\" " + goliathNameClan[Math.floor(Math.random() * goliathNameClan.length)];
                break;

            case HILL_DWARF:
                c.stats.put(CONSTITUTION, 2);
                c.stats.put(WISDOM, 1);


                c.lang.add("Dwarvish");
                c.move = 25;
                c.abil.add("Darkvision");
                c.abil.add("Dwarven Resilience");
                c.abil.add("Stonecunning");
                c.abil.add("Dwarven toughness");
                int x = Roll.d(3) - 1;
                switch (x) {
                    case (0):
                        c.tools.add("Smith's tools");
                        break;
                    case (1):
                        c.tools.add("Brewer's tools");
                        break;
                    case (2):
                        c.tools.add("Mason's tools");
                        break;
                }

//                if (gender==2) {
//                    name = dwarfNameF[Math.floor(Math.random() * dwarfNameF.length)];
//                } else {
//                    name = dwarfNameM[Math.floor(Math.random() * dwarfNameM.length)];
//                }
//                name += " " + dwarfNameLast[Math.floor(Math.random() * dwarfNameLast.length)];
                break;

            case MOUNTAIN_DWARF:
                c.stats.put(STRENGTH, 2);
                c.stats.put(CONSTITUTION, 2);


                c.lang.add("Dwarvish");
                c.move = 25;
                c.abil.add("Darkvision");
                c.abil.add("Dwarven Resilience");
                c.abil.add("Stonecunning");
                x = Roll.d(3) - 1;
                switch (x) {
                    case (0):
                        c.tools.add("Smith's tools");
                        break;
                    case (1):
                        c.tools.add("Brewer's tools");
                        break;
                    case (2):
                        c.tools.add("Mason's tools");
                        break;
                }
//                if (gender==2) {
//                    name = dwarfNameF[Math.floor(Math.random() * dwarfNameF.length)];
//                } else {
//                    name = dwarfNameM[Math.floor(Math.random() * dwarfNameM.length)];
//                }
//                name += " " + dwarfNameLast[Math.floor(Math.random() * dwarfNameLast.length)];
                break;
//
            case HUMAN:
                c.stats.put(STRENGTH, 1);
                c.stats.put(DEXTERITY, 1);
                c.stats.put(CONSTITUTION, 1);
                c.stats.put(INTELLIGENCE, 1);
                c.stats.put(WISDOM, 1);
                c.stats.put(CHARISMA, 1);
                c.move = 30;
                c.extraLanguages += 1;
//                if (gender==2) {
//                    name = humanNameF[Math.floor(Math.random() * humanNameF.length)];
//                } else {
//                    name = humanNameM[Math.floor(Math.random() * humanNameM.length)];
//                }
//                name += " " + humanNameLast[Math.floor(Math.random() * humanNameLast.length)];
                break;
//
            case HIGH_ELF:
                c.stats.put(DEXTERITY, 2);
                c.stats.put(INTELLIGENCE, 1);
                c.lang.add("Elvish");
                c.raceSpellAbility = INTELLIGENCE;
                c.move = 30;
                c.extraLanguages += 1;
//                Skills.giveProf(sk.perc);
                c.skills.put(PERCEPTION, 1);
                c.abil.add("Darkvision");
                c.abil.add("Keen senses");
                c.abil.add("Fey ancestry");
                c.abil.add("Trance");
                c.abil.add("Cantrip");
                c.abil.add("Extra Language");
                c.raceSpellAbility = INTELLIGENCE;
                // add cantrip
//                if (gender==2) {
//                    name = elfNameF[Math.floor(Math.random() * elfNameF.length)];
//                } else {
//                    name = elfNameM[Math.floor(Math.random() * elfNameM.length)];
//                }
//                name += " " + elfNameLast[Math.floor(Math.random() * elfNameLast.length)];
                break;
            case WOOD_ELF:
                c.stats.put(DEXTERITY, 2);
                c.stats.put(WISDOM, 1);
                c.move = 35;
                c.lang.add("Elvish");
                c.abil.add("Darkvision");
                c.abil.add("Keen senses");
                c.abil.add("Fey ancestry");
                c.abil.add("Trance");
                c.abil.add("Mask of the Wild");
//                Skills.giveProf(sk.perc);
                c.skills.put(PERCEPTION, 1);
//                if (gender==2) {
//                    name = elfNameF[Math.floor(Math.random() * elfNameF.length)];
//                } else {
//                    name = elfNameM[Math.floor(Math.random() * elfNameM.length)];
//                }
//                name += " " + elfNameLast[Math.floor(Math.random() * elfNameLast.length)];
                break;

            case STOUT_HALFLING:
                c.stats.put(DEXTERITY, 2);
                c.stats.put(CONSTITUTION, 1);
                c.size = "Small";
                c.lang.add("Halfling");
                c.abil.add("Lucky");
                c.abil.add("Brave");
                c.abil.add("Halfling Nimbleness");
                c.abil.add("Stout Resilience");
                c.move = 25;
//                if (gender==2) {
//                    name = halfNameF[Math.floor(Math.random() * halfNameF.length)];
//                } else {
//                    name = halfNameM[Math.floor(Math.random() * halfNameM.length)];
//                }
//                name += " " + halfNameLast[Math.floor(Math.random() * halfNameLast.length)];
                break;
            case LIGHTFOOT_HALFLING:
                c.stats.put(DEXTERITY, 2);
                c.stats.put(CHARISMA, 1);
                c.size = "Small";
                c.lang.add("Halfling");
                c.abil.add("Lucky");
                c.abil.add("Brave");
                c.abil.add("Halfling Nimbleness");
                c.abil.add("Naturally Stealthy");
                c.move = 25;
//                if (gender==2) {
//                    name = halfNameF[Math.floor(Math.random() * halfNameF.length)];
//                } else {
//                    name = halfNameM[Math.floor(Math.random() * halfNameM.length)];
//                }
//                name += " " + halfNameLast[Math.floor(Math.random() * halfNameLast.length)];
                break;
        }
    }

    /**
     * Only used by half-elves to increase 2 random stats by one.
     * Since charisma was already increased it cannot be increased again
     *
     * @return Map of updated stats.
     */
    private static Map<Stats, Integer> halfElfIncreaseTwoStats(Character c) {
        Map<Stats, Integer> increased = new HashMap<Stats, Integer>();
        while (increased.size() < 2) {
            // randomly generate int from 0-4 (not 5 because charisma already was increased);
            int statNum = Roll.d(5) - 1;
            Stats statToIncrement = Stats.values()[statNum];
            Integer preIncrementValue = c.stats.get(statToIncrement);
            preIncrementValue++;
            increased.put(statToIncrement, preIncrementValue);
        }
        // now fill in the rest
        for (Stats stat : c.stats.keySet()) {
            if (!increased.containsKey(stat)) {
                increased.put(stat, c.stats.get(stat));
            }
        }
        System.out.println(increased);
        return increased;
    }

    private static void addDragonbornAbilities(Character c) {
        int x = Roll.d(10) - 1;
        String[] dragon = new String[0];
        switch (x) {
            case (0):
                dragon = new String[]{"Black", "Acid", "5 x 30 ft. line"};
                break;
            case (1):
                dragon = new String[]{"Blue", "Lightning", "5 x 30 ft. line"};
                break;
            case (2):
                dragon = new String[]{"Brass", "Fire", "5 x 30 ft. line"};
                break;
            case (3):
                dragon = new String[]{"Bronze", "Lightning", "5 x 30 ft. line"};
                break;
            case (4):
                dragon = new String[]{"Copper", "Acid", "5 x 30 ft. line"};
                break;
            case (5):
                dragon = new String[]{"Gold", "Fire", "15 ft. cone"};
                break;
            case (6):
                dragon = new String[]{"Green", "Poison", "15 ft. cone"};
                break;
            case (7):
                dragon = new String[]{"Red", "Fire", "15 ft. cone"};
                break;
            case (8):
                dragon = new String[]{"Silver", "Cold", "15 ft. cone"};
                break;
            case (9):
                dragon = new String[]{"White", "Cold", "15 ft. cone"};
                break;
        }
        c.abil.add("Draconic Ancestry (" + dragon[0] + ")");
        c.abil.add("Breath Weapon (" + dragon[1] + " damage in a " + dragon[2] + ")");
        c.abil.add("Damage Resistance (" + dragon[1] + ")");
    }
}