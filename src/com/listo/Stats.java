package com.listo;

import com.listo.Util.Util;

import java.util.*;

public enum Stats {

    STRENGTH, DEXTERITY, CONSTITUTION, INTELLIGENCE, WISDOM, CHARISMA;

    public static List<Integer> getRawRolls() {
//        int[] raw = {  };
        List<Integer> raw = new LinkedList<Integer>(Arrays.asList(Roll.best3of4(), Roll.best3of4(), Roll.best3of4(),
                Roll.best3of4(), Roll.best3of4(), Roll.best3of4()));
        // order raw rolls starting with best roll and ending with worst
        Collections.sort(raw);
        Collections.reverse(raw);
        return raw;

    }

    public static Integer assignIfEmpty(Integer stat, List<Integer> raw) {
        if (stat < 3) {
            stat += raw.remove(0);
            return stat;
        }
        return stat;
    }

    public static void calculateSavingThrows(Character c) {
        for (Stats stat : c.savingThrows.keySet()) {
            int prof=0;
            // add proficiency bonus if you are proficient with the save
            if (c.savingThrowProficiencies.get(stat)) {
                prof=c.prof;
            }
            // final result is ability modifier, plus proficiency if you're proficient
            c.savingThrows.put(stat, Util.abilityModifier(c.stats.get(stat)) + prof);
        }
    }

    public static Map<Stats, Integer> assignStats(Map<Stats, Integer> stats, List<Stats> preferredStats, List<Integer> raw) {
        for (Stats stat : preferredStats) {
            stats.put(stat, stats.get(stat)+raw.remove(0));
        }
        System.out.println(" class assigned these:  "+stats);

        // now assign everything not in preferredStats
        // everyone needs a good con, so assign that first
        stats.put(CONSTITUTION, assignIfEmpty(stats.get(CONSTITUTION), raw));
        // from now on every assignment will be randomly. order doesn't matter
        Collections.shuffle(raw);
        for (Stats stat : stats.keySet()) {
            stats.put(stat, assignIfEmpty(stats.get(stat), raw));
        }
        return stats;
    }


    }


