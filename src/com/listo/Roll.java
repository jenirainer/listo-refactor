package com.listo;

import java.util.Arrays;

public class Roll {

    public static int d (int dice, int sides) {
        int total = 0;
        for (int i=0; i < dice; i++) {

            total += (Math.random() * sides) +1;
        }
        System.out.println(total);
        return total;
    }

    public static int d (int sides) {
        return (int) ((Math.random() * sides) +1);
    }

    public static int best3of4 () {
        int[] x = { d(6), d(6), d(6), d(6)};
        Arrays.sort(x);
//        System.out.println(x[0]);
//        System.out.println(x[1]);
//        System.out.println(x[2]);
//        System.out.println(x[3]);
        return x[1] + x[2] + x[3];
    }
}