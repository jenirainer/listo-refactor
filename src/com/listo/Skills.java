package com.listo;

import com.listo.Util.Util;

import java.util.List;

import static com.listo.Stats.*;


//public enum Skills {
//    ACROBATICS(DEXTERITY, "Acrobatics", 0, 0),    // stat string know_it? final_value
//    ANIMAL_HANDLING(WISDOM, "Animal Handling", 0),
//    ARCANA(INTELLIGENCE, 0),
//    ATHLETICS(STRENGTH, 0),
//    DECEPTION(CHARISMA, 0),
//    HISTORY(INTELLIGENCE, 0),
//    INSIGHT(WISDOM, 0),
//    INTIMIDATION(CHARISMA, 0),
//    INVESTIGATION(INTELLIGENCE, 0),
//    MEDICINE(WISDOM, 0),
//    NATURE(INTELLIGENCE, 0),
//    PERCEPTION(WISDOM, 0),
//    PERFORMANCE(CHARISMA, 0),
//    PERSUASION(CHARISMA, 0),
//    RELIGION(INTELLIGENCE, 0),
//    SLEIGHT_OF_HAND(DEXTERITY, 0),
//    STEALTH(DEXTERITY, 0),
//    SURVIVAL(WISDOM, 0);
//
//    private final Stats skillStat;
//    private int value;
//    Skills(Stats skillStat, int value) {
//        this.skillStat = skillStat;
//        this.value = value;
//    }


public enum Skills {
    ACROBATICS(DEXTERITY),
    ANIMAL_HANDLING(WISDOM),
    ARCANA(INTELLIGENCE),
    ATHLETICS(STRENGTH),
    DECEPTION(CHARISMA),
    HISTORY(INTELLIGENCE),
    INSIGHT(WISDOM),
    INTIMIDATION(CHARISMA),
    INVESTIGATION(INTELLIGENCE),
    MEDICINE(WISDOM),
    NATURE(INTELLIGENCE),
    PERCEPTION(WISDOM),
    PERFORMANCE(CHARISMA),
    PERSUASION(CHARISMA),
    RELIGION(INTELLIGENCE),
    SLEIGHT_OF_HAND(DEXTERITY),
    STEALTH(DEXTERITY),
    SURVIVAL(WISDOM);

    private final Stats skillStat;
    Skills(Stats skillStat) {
        this.skillStat = skillStat;
    }

    public static void addSkills(Character c, int choicesLeft, List<Skills> choices) {
        while (choicesLeft > 0) {
            int x = Roll.d(choices.size())-1;
            Skills randomSkill = choices.get(x);
            if (c.skills.get(randomSkill) == 0) {
               System.out.println(randomSkill + " added");
                c.skills.put(randomSkill, 1);
                choicesLeft--;
            } else {
                System.out.println("we already know " + randomSkill);
            }
        }
    }

    public Stats getSkillStat() {
        return skillStat;
    }

    public static String printOneSkill(Character c, Skills skill) {
        int x = c.skills.get(skill);
//        System.out.println(x);
        x = x * c.prof;
//        System.out.println(x);
        x += Util.abilityModifier(c.stats.get(skill.getSkillStat()));
//        System.out.println(Util.showPlus(x));
        return c.skills.get(skill)+" "+Util.showPlus(x);
    }

    public static String printSkills (Character c) {
        String result="";
        for (Skills skill : c.skills.keySet()) {
            result += skill + " " + printOneSkill(c, skill) +", ";
        }
        return result;
    }
}
