package com.listo;

import com.listo.Util.Const;
import com.listo.Util.Util;

import java.util.*;

import static com.listo.Skills.*;
import static com.listo.Stats.*;

public class Character {
    public Integer lvl=0;
    protected Map<Stats, Integer> stats;
    protected Map<Stats, Boolean> savingThrowProficiencies;
    protected Map<Stats, Integer> savingThrows;
    protected List<Stats> preferredStats;
    public Stats spellAbility;
    public Stats raceSpellAbility;

    protected Map<Skills, Integer> skills;
    protected Boolean hasFamiliar;



    public String name;
    protected Race race;
//    protected Race race2;
    public String background;
    public String ideal;
    public String bond;
    public String flaw;
    public String lifestyle;

    public String size="Medium";
    public String charClass;
    public String archetype="";
    public String gender;
    public String suggestedBackground;

    public int hp=0;
    public int hd=0;
    public int move=0;
    public int ac=10;
    public int prof=2;
    public int gold=0;

    public int extraLanguages=0;
    public int extraSkills=0;

    public List<String> abil = new LinkedList<String>();
    public List<String> lang = new LinkedList<String>();
    public List<String> equipment = new LinkedList<String>();
    public List<String> raceSpells = new LinkedList<String>();
    public List<String> tools = new LinkedList<String>();
    public List<String> profList = new LinkedList<String>();
    public List<String> nonProfList = new LinkedList<String>();
    public List<String> personality = new LinkedList<>();

//    public Character() {
//        race_old = Race_Old.selectRace();
//        charClass = CharClass.selectClass();
//        suggestedBackground = CharClass.getSuggestedBackground(charClass);
//    }

    public Character(String gender, Race race, String charClass, String background, int lvl) {
        stats = new HashMap<Stats, Integer>();
        stats.put(CHARISMA, 0);
        stats.put(CONSTITUTION, 0);
        stats.put(DEXTERITY, 0);
        stats.put(INTELLIGENCE, 0);
        stats.put(STRENGTH, 0);
        stats.put(WISDOM, 0);
        savingThrows = new HashMap<Stats, Integer>();
        savingThrows.put(CHARISMA, 0);
        savingThrows.put(CONSTITUTION, 0);
        savingThrows.put(DEXTERITY, 0);
        savingThrows.put(INTELLIGENCE, 0);
        savingThrows.put(STRENGTH, 0);
        savingThrows.put(WISDOM, 0);
        savingThrowProficiencies = new HashMap<Stats, Boolean>();
        savingThrowProficiencies.put(CHARISMA, false);
        savingThrowProficiencies.put(CONSTITUTION, false);
        savingThrowProficiencies.put(DEXTERITY, false);
        savingThrowProficiencies.put(INTELLIGENCE, false);
        savingThrowProficiencies.put(STRENGTH, false);
        savingThrowProficiencies.put(WISDOM, false);
//        for (Stats stat : savingThrows.keySet()) {
//            savingThrows.put(stat, 0);
//        }

        // set all saving throw proficiencies to false. class sets these later
//        for (Stats stat : savingThrowProficiencies.keySet()) {
//            savingThrowProficiencies.put(stat, false);
//        }
//        for (Stats stat : stats.keySet()) {
//            this.stats.put(stat, 0);
//            System.out.println("new val="+this.stats.get(stat));
//        }
        skills = new HashMap<Skills, Integer>();
        skills.put(ACROBATICS, 0);
        skills.put(ANIMAL_HANDLING, 0);
        skills.put(ARCANA, 0);
        skills.put(ATHLETICS, 0);
        skills.put(DECEPTION, 0);
        skills.put(HISTORY, 0);
        skills.put(INSIGHT, 0);
        skills.put(INTIMIDATION, 0);
        skills.put(INVESTIGATION, 0);
        skills.put(MEDICINE, 0);
        skills.put(NATURE, 0);
        skills.put(PERCEPTION, 0);
        skills.put(PERFORMANCE, 0);
        skills.put(PERSUASION, 0);
        skills.put(RELIGION, 0);
        skills.put(SLEIGHT_OF_HAND, 0);
        skills.put(STEALTH, 0);
        skills.put(SURVIVAL, 0);
        if (gender.length() == 0) {
            if (Roll.d(2) == 2) {
                this.gender="Male";
            } else {
                this.gender="Female";
            }
        } else {
            this.gender=gender;
        }
        if (race == null) {
            this.race = Race.selectRace();
        } else {
            this.race = race;
        }
        if (charClass.length() == 0) {
            this.charClass = CharClass.selectClass();
        } else {
            this.charClass = charClass;
        }
        if (background.length() > 0) {
            this.background = background;
        } else {
            this.background = "";
        }

        if (lvl > 0) {
            this.lvl = lvl;
        }
        else {
            this.lvl = Roll.d(10);
        }
    }


    public static void setProf(Character c) {
        if (c.lvl >= 5) {
            c.prof=3;
        }
        if (c.lvl >= 9) {
            c.prof=4;
        }
    }

    public static void printCharacter(Character c) {
        System.out.println("HP " + c.hp + " / Prof bonus: " + Util.showPlus(c.prof));

        System.out.println("Level " + c.lvl + " " + c.race + " " + c.charClass + "/" + c.archetype + " " + c.gender + " "+ c.background);
        System.out.println("Personality: "+c.personality + " Flaw: " + c.flaw + " Bond: " + c.bond + " Ideal: "+c.ideal);
//        System.out.println(charClass.move);
//        System.out.println(charClass.charClass);
        System.out.println("NEW=== STR: "+ c.stats.get(STRENGTH) + " DEX: "+ c.stats.get(DEXTERITY) + " CON: "+ c.stats.get(CONSTITUTION) + " WIS: "+ c.stats.get(WISDOM) + " INT: "+ c.stats.get(INTELLIGENCE) + " CHA: "+ c.stats.get(CHARISMA) );

//        System.out.println("STR: "+ c.st + " DEX: "+ c.de + " CON: "+ c.co + " WIS: "+ c.wi + " INT: "+ c.in  + " CHA: "+ c.ch  );
        System.out.println("Saving throws:  STR: "+ Util.showPlus(c.savingThrows.get(STRENGTH)) + " DEX: "+ Util.showPlus(c.savingThrows.get(DEXTERITY)) + " CON: "+Util.showPlus(c.savingThrows.get(CONSTITUTION)) + " WIS: "+ Util.showPlus(c.savingThrows.get(WISDOM)) + " INT: "+ Util.showPlus(c.savingThrows.get(INTELLIGENCE)) + " CHA: "+ Util.showPlus(c.savingThrows.get(CHARISMA)));
        System.out.println("Abilities: " +Util.displayList((c.abil)));
        System.out.println("Languages: "+ Util.displayList(c.lang));
        System.out.println("Move: "+c.move +" / "+c.size);
//        sk.acro.value='y';
        System.out.println(Skills.printSkills(c));
//        Skills.printSkills(c, sk);
        System.out.println("Equipment: "+ Util.displayList(c.equipment));
        if (c.tools.size() > 0) {
            System.out.println("Tool proficiencies: " + Util.displayList(c.tools));
        }
        if (c.raceSpells.size() > 0) {
            System.out.println("Race spells: " + Util.displayList(c.raceSpells) + " Race spell ability: "+c.raceSpellAbility);
        }

    }


    public static void calculateHitPoints(Character c) {
        int hp=0;
        //max 1st level hp
        hp += c.hd;
//        imt r=0;
        String log ="";
        double total=0;
        for (int i=2; i <= c.lvl; i++) {
            int r=Roll.d(c.hd);
            hp += r;
            total += r;
            log += "level "+i+" hp roll="+r+ " newhp="+hp+"/";
        }
        if (c.lvl > 1)
            log += "/ average roll = "+ (double) (total/(c.lvl-1))+"/";
        int bonus = Util.abilityModifier(c.stats.get(CONSTITUTION));
        if (bonus > 0) {
            bonus *= c.lvl;
            hp += bonus;
            log += " con bonus "+bonus+"/";
        }
        // dragon sorc bonus
        if(c.race.equals("Hill Dwarf")) {
            hp +=  c.lvl;
            log += "hill dwarf +"+c.lvl;
        }
        c.hp = hp;

        log += "/ hp="+c.hp;
        System.out.println(log);
    }


    public static void addLanguages(Character c) {
        String[] exotics = new String[]{"Abyssal","Celestial","Draconic","Deep Speech","Infernal","Primordial","Sylvan","Undercommon"};
        String[] langs=new String[]{"Dwarvish","Elvish","Giant","Goblin","Gnomish","Halfling","Orc"};
        String pick;
        while (c.extraLanguages > 0) {
            // 1 in 9 chance of knowing exotic language instead of normal one. 2 in 9 for sages
            // TODO knowledge clerics more exotgics?
            int x=Roll.d(9);
            if (c.background.contains("Sage")) {
                x+=1;
            }
            if (x >= 9) {
                pick=Util.pickOne(exotics);
            } else {
                pick=Util.pickOne(langs);
            }
            // only add it if we don't know it
            if (Util.addIfNone(c.lang, pick)) {
                c.extraLanguages--;
            }
        }
    }


    public static void addMusic(Character c, int howMany, boolean addEquipment) {
        while (howMany > 0) {
            String m = Util.pickOne(Const.music);
            if (c.tools.contains(m)) {
                System.out.println("already have this instrument");
            } else {
                howMany--;
                c.tools.add(m);
                if (addEquipment) {
                    c.equipment.add(m);
                }
            }
        }
    }

    public static void addTools(Character c, int howMany, boolean addEquipment, boolean isGuildArtisan) {
        while (howMany > 0) {
            String tool = Util.pickOne(Const.artisan);
            if (c.tools.contains(tool)) {
                System.out.println("already have this tool");
            } else {
                howMany--;
                c.tools.add(tool+"'s tools");
                if (addEquipment) {
                    c.equipment.add(tool + "'s tools");
                }
                if (isGuildArtisan) {
                    c.background += " (" + tool + ")";
                }
            }
        }
    }

    public static void addTools(Character c, int howMany, boolean addEquipment) {
        addTools(c,howMany,addEquipment,false);
    }


}
