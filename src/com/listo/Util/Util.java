package com.listo.Util;

import com.listo.Roll;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;

public class Util {

    public static String pickOne(String[] array) {
        int roll= Roll.d(array.length)-1;
        return array[roll];
    }

    public static String pickOneWeighted(String[] words, int[] weights) {

        int total=0;
        for (int i=0; i < weights.length; i++) {
            total+=weights[i];
            System.out.println(words[i] + "->" + weights[i]);
        }
        int target=Roll.d(total);
//        System.out.println("tgarget="+target+" total="+total);
        int iteratingTotal=0;
        for (int i=0; i < weights.length;i++) {

//            System.out.println("i="+i+" iterating total="+iteratingTotal+ " "+words[i]+" " + weights[i]);
            if ((iteratingTotal+weights[i]) >= target) {
                System.out.println("Winner = "+words[i]);
                return words[i];
            }
            iteratingTotal+=weights[i];
        }

//        int roll= Roll.d(array.length)-1;
//
//        for (int item.weight : array) {
//            total+=item.weight;
//        }
        return "fail weighted pick";
    }

    public static List<String> pickMultiple(String[] array, int n) {
        List<String> result = new ArrayList<>();
        result.add(pickOne(array));
        int added=1;
        while (added < n) {
            String pick = pickOne(array);
            if (!result.contains(pick)) {
                added++;
                result.add(pick);
            }
        }
//        System.out.println("All added.  n="+n);
        return result;
    }



    public static boolean addIfNone(List values, String newValue) {
        if (values.contains(newValue)) {
            return false;
        } else {
            values.add(newValue);
            return true;
        }
    }


    public static String showPlus(int x) {
        if (x>0) {
            return ("+"+x);
        } else {
            return ("" + x);
        }
    }

    public static String displayList(List<String> list) {
        String result = "";
        for (String l : list) {
            result += l + ", ";
        }
        // trim away last comma
        result = result.substring(0,result.length()-2);
        return result;
    }

    public static int abilityModifier(int ab) {
        if (ab <= 3) {
            return -4;
        }
        if (ab <= 5) {
            return -3;
        }
        if (ab <= 7) {
            return -2;
        }
        if (ab <= 9) {
            return -1;
        }
        if (ab <= 11) {
            return 0;
        }
        if (ab <= 13) {
            return 1;
        }
        if (ab <= 15) {
            return 2;
        }
        if (ab <= 17) {
            return 3;
        }
        if (ab <= 19) {
            return 4;
        }
        if (ab <= 21) {
            return 5;
        }
        return 0;
    }

    public static <T extends Enum<?>> T randomEnum(Class<T> clazz){
        SecureRandom random = new SecureRandom();
        int x = random.nextInt(clazz.getEnumConstants().length);
        return clazz.getEnumConstants()[x];
    }

}
