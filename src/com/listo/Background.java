package com.listo;

import com.listo.Util.Const;
import com.listo.Util.Util;

import static com.listo.Skills.*;

public class Background {
    String name;
        	 
    public static void selectBackground(Character c) {
        if (c.background.length() > 0) {
            return;
        }
        // 40% CHANCE of going w/ default bg
        int x=Roll.d(10);
        if (x <= 4) {
            System.out.println("default background!");
            c.background=c.suggestedBackground;
        } else {
            String[] bgs = new String[]{"Acolyte", "Charlatan", "Criminal", "Entertainer", "Folk Hero", "Guild Artisan", "Hermit", "Noble", "Outlander", "Sage", "Sailor", "Soldier", "Urchin"};
            c.background= Util.pickOne(bgs);
        }
    }

    public static String getSuggestedBackground(String c) {
        switch (c) {

            case ("Barbarian"):
                return "Outlander";

            case ("Bard"):
                return "Entertainer";

            case ("Cleric"):
                return "Acolyte";

            case ("Druid"):
                return "Hermit";

            case ("Fighter (Eldritch Knight)"):
                return "Soldier";

            case ("Fighter (High Dexterity)"):
                return "Soldier";

            case ("Fighter (High Strength)"):
                return "Soldier";

            case ("Monk"):
                return "Hermit";

            case ("Paladin"):
                return "Noble";

            case ("Ranger"):
                return "Outlander";

            case ("Rogue (Thief)"):
                int x=Roll.d(3);
                if (x==1) return "Charlatan";
                if (x==2) return "Urchin";
                if (x==3) return "Criminal";

            case("Rogue (Assassin)"):
                x=Roll.d(3);
                if (x==1) return "Charlatan";
                if (x==2) return "Urchin";
                if (x==3) return "Criminal";

            case("Rogue (Arcane Trickster)"):
                x=Roll.d(3);
                if (x==1) return "Charlatan";
                if (x==2) return "Urchin";
                if (x==3) return "Criminal";

            case ("Sorcerer"):
                return "Hermit";

            case ("Warlock"):
                return "Charlatan";

            case ("Wizard"):
                return "Sage";

            default:
                return "";

        }

    }

    public static void backgroundAdjustments(Character c) {

//personality, ideal, bond, flaw

        switch (c.background) {
            case("Acolyte"):
                c.personality=Util.pickMultiple(Const.acolytePersonality, 2);
                c.ideal=Util.pickOne(Const.acolyteIdeal);
                c.flaw=Util.pickOne(Const.acolyteFlaw);
                c.bond=Util.pickOne(Const.acolyteBond);
                c.skills.put(INSIGHT, 1);
                c.skills.put(RELIGION, 1);

                c.extraLanguages += 2;
                c.lifestyle = "";
                Util.addIfNone(c.equipment, "Holy symbol");
                c.equipment.add("Prayer book");
                c.equipment.add("Common clothes");
                c.gold += 15;
                break;
            case("Charlatan"):
                c.personality=Util.pickMultiple(Const.charlatanPersonality, 2);
                c.ideal=Util.pickOne(Const.charlatanIdeal);
                c.flaw=Util.pickOne(Const.charlatanFlaw);
                c.bond=Util.pickOne(Const.charlatanBond);
                c.skills.put(DECEPTION, 1);
                c.skills.put(SLEIGHT_OF_HAND, 1);
                int x=Roll.d(6)-1;
                if (x==0)
                    c.background += " (I cheat at games of chance.)";
                if (x==1)
                    c.background += " (I shave coins or forge documents.)";
                if (x==2)
                    c.background += " (I insinuate myself into people’s lives to prey on their weakness and secure their fortunes.)";
                if (x==3)
                    c.background += " (I put on new identities like clothes.)";
                if (x==4)
                    c.background += " (I run sleight-of-hand cons on street corners.)";
                if (x==5)
                    c.background += " (I convince people that worthless junk is worth their hard-earned money.)";
                c.equipment.add("Fine clothes");
                c.equipment.add("Disguise kit");
                c.equipment.add("Weighted dice");
                c.tools.add("Disguise kit");
                c.tools.add("Forgery kit");
                c.gold += 15;
                break;
            case("Criminal"):
                c.personality=Util.pickMultiple(Const.criminalPersonality, 2);
                c.ideal=Util.pickOne(Const.criminalIdeal);
                c.flaw=Util.pickOne(Const.criminalFlaw);
                c.bond=Util.pickOne(Const.criminalBond);
                c.skills.put(DECEPTION, 1);
                c.skills.put(STEALTH, 1);

                c.gold+=15;
                c.equipment.add("Crowbar");
                c.equipment.add("Dark common clothes");
                c.tools.add(Util.pickOne(Const.gaming));
                c.tools.add("Thieves' tools");
                //        myLifestyle = 2;
                x = Roll.d(8)-1;
                if (x==0)
                    c.background += " (Blackmailer)";
                if (x==1)
                    c.background += " (Burglar)";
                if (x==2)
                    c.background += " (Enforcer)";
                if (x==3)
                    c.background += " (Fence)";
                if (x==4)
                    c.background += " (Highway robber)";
                if (x==5)
                    c.background += " (Hired Killer)";
                if (x==6)
                    c.background += " (Pickpocket)";
                if (x==7)
                    c.background += " (Smuggler)";
                break;
            case("Entertainer"):
                c.personality=Util.pickMultiple(Const.entertainerPersonality, 2);
                c.ideal=Util.pickOne(Const.entertainerIdeal);
                c.flaw=Util.pickOne(Const.entertainerFlaw);
                c.bond=Util.pickOne(Const.entertainerBond);
                c.equipment.add("Costume");
                c.skills.put(ACROBATICS, 1);
                c.skills.put(PERFORMANCE, 1);
//        myLifestyle = 3;
                Character.addMusic(c,1,true);
                c.tools.add("Disguise kit");
                c.gold += 15;
                break;

            case("Folk Hero"):
                c.personality=Util.pickMultiple(Const.folkHeroPersonality, 2);
                c.ideal=Util.pickOne(Const.folkHeroIdeal);
                c.flaw=Util.pickOne(Const.folkHeroFlaw);
                c.bond=Util.pickOne(Const.folkHeroBond);
                c.equipment.add("Common clothes");
                c.tools.add("Land vehicles");
                Character.addTools(c,1,true);
                c.skills.put(ANIMAL_HANDLING, 1);
                c.skills.put(SURVIVAL, 1);

//        myLifestyle = 3;

                c.gold += 10;
                x=Roll.d(10)-1;
                if (x==0)
                    c.background += " (I stood up to a tyrant’s agents.)";
                if (x==1)
                    c.background += " (I saved people during a natural disaster.)";
                if (x==2)
                    c.background += " (I stood alone against a terrible monster.)";
                if (x==3)
                    c.background += " (I stole from a corrupt merchant to help the poor.)";
                 if (x==4)
                    c.background += " (I led a militia to fight off an invading army.)";
                if (x==5)
                    c.background += " (I broke into a tyrant’s castle and stole weapons to arm the people.)";
                if (x==6)
                    c.background += " (I trained the peasantry to use farm implements as weapons against a tyrant’s soldiers.)";
                if (x==7)
                    c.background += " (A lord rescinded an unpopular decree after I led a symbolic act of protest against it.)";
                if (x==8)
                    c.background += " (A celestial, fey, or similar creature gave me a blessing or revealed my secret origin.)";
                if (x==9)
                    c.background += " (Recruited into a lord’s army, I rose to leadership and was commended for my heroism.)";
                break;
            case("Guild Artisan"):
                c.personality=Util.pickMultiple(Const.guildArtisanPersonality, 2);
                c.ideal=Util.pickOne(Const.guildArtisanIdeal);
                c.flaw=Util.pickOne(Const.guildArtisanFlaw);
                c.bond=Util.pickOne(Const.guildArtisanBond);
                c.equipment.add("Guild letter");
                c.equipment.add("Traveler's clothes");
                c.skills.put(INSIGHT, 1);
                c.skills.put(PERSUASION, 1);

                Character.addTools(c, 1, true, true);
//        myLifestyle = 4;
                c.extraLanguages += 1;
                c.gold += 15;
                break;
            case("Hermit"):
                c.personality=Util.pickMultiple(Const.hermitPersonality, 2);
                c.ideal=Util.pickOne(Const.hermitIdeal);
                c.flaw=Util.pickOne(Const.hermitFlaw);
                c.bond=Util.pickOne(Const.hermitBond);
                c.equipment.add("Record of Hermit studies");
                c.equipment.add("Common clothes");
                c.equipment.add("Herbalism kit");
                c.skills.put(MEDICINE, 1);
                c.skills.put(RELIGION, 1);
                Util.addIfNone(c.tools, "Herbalism kit");
        //        myLifestyle = 2;
                c.gold += 5;
                c.extraLanguages += 1;
                x=Roll.d(8)-1;
                if (x==0)
                    c.background += " (I was searching for spiritual enlightenment.)";
                if (x==1)
                    c.background += " (I was partaking of communal living in accordance with the dictates of a religious order.)";
                if (x==2)
                    c.background += " (I was exiled for a crime I didn’t commit.)";
                if (x==3)
                    c.background += " (I retreated from society after a life-altering event.)";
                if (x==4)
                    c.background += " (I needed a quiet place to work on my art, literature, music, or manifesto.)";
                if (x==5)
                    c.background += " (I needed to commune with nature, far from civilization.)";
                if (x==6)
                    c.background += " (I was the caretaker of an ancient ruin or relic.)";
                if (x==7)
                    c.background += " (I was a pilgrim in search of a person, place, or relic of spiritual significance.)";
                break;
            case("Noble"):
                c.personality=Util.pickMultiple(Const.noblePersonality, 2);
                c.ideal=Util.pickOne(Const.nobleIdeal);
                c.flaw=Util.pickOne(Const.nobleFlaw);
                c.bond=Util.pickOne(Const.nobleBond);
                c.equipment.add("Fine clothes");
                c.equipment.add("Signet ring");
                c.equipment.add("Scroll of pedigree");
                c.skills.put(HISTORY, 1);
                c.skills.put(PERSUASION, 1);

                c.tools.add(Util.pickOne(Const.gaming));
                c.gold += 25;
                c.extraLanguages += 1;
                break;
            case("Outlander"):
                c.personality=Util.pickMultiple(Const.outlanderPersonality, 2);
                c.ideal=Util.pickOne(Const.outlanderIdeal);
                c.flaw=Util.pickOne(Const.outlanderFlaw);
                c.bond=Util.pickOne(Const.outlanderBond);
                c.equipment.add("Hunting trap");
                c.equipment.add("Animal trophy");
                c.equipment.add("Traveler's clothes");
                c.skills.put(ATHLETICS, 1);
                c.skills.put(SURVIVAL, 1);

                Character.addTools(c, 1, false);
//        myLifestyle = 2;
                c.gold += 10;
                c.extraLanguages += 1;
                x= Roll.d(10)-1;
                if (x==0)
                    c.background += " (Forester)";
                if (x==1)
                    c.background += " (Trapper)";
                if (x==2)
                    c.background += " (Homesteader)";
                if (x==3)
                    c.background += " (Guide)";
                if (x==4)
                    c.background += " (Exile or outcast)";
                if (x==5)
                    c.background += " (Bounty hunter)";
                if (x==6)
                    c.background += " (Pilgrim)";
                if (x==7)
                    c.background += " (Tribal nomad)";
                if (x==8)
                    c.background += " (Hunter-gatherer)";
                if (x==9)
                    c.background += " (Tribal marauder)";
                break;
            case("Sage"):
                c.personality=Util.pickMultiple(Const.sagePersonality, 2);
                c.ideal=Util.pickOne(Const.sageIdeal);
                c.flaw=Util.pickOne(Const.sageFlaw);
                c.bond=Util.pickOne(Const.sageBond);
                c.equipment.add("Black ink");
                c.equipment.add("Quill");
                c.equipment.add("Colleague's letter");
                c.equipment.add("Common clothes");
                c.skills.put(ARCANA, 1);
                c.skills.put(HISTORY, 1);

//        myLifestyle = 3;
                c.extraLanguages += 2;
                c.gold += 10;
                x=Roll.d(8)-1;
                if (x==0)
                    c.background += " (Alchemist)";
                if (x==1)
                    c.background += " (Astronomer)";
                if (x==2)
                    c.background += " (Discredited academic)";
                if (x==3)
                    c.background += " (Librarian)";
                if (x==4)
                    c.background += " (Professor)";
                if (x==5)
                    c.background += " (Researcher)";
                if (x==6)
                    c.background += " (Wizard's apprentice)";
                if (x==7)
                    c.background += " (Scribe)";

                break;
            case("Sailor"):
                c.personality=Util.pickMultiple(Const.sailorPersonality, 2);
                c.ideal=Util.pickOne(Const.sailorIdeal);
                c.flaw=Util.pickOne(Const.sailorFlaw);
                c.bond=Util.pickOne(Const.sailorBond);
                c.equipment.add("50 feet silk rope");
                c.equipment.add("Lucky charm");

                c.equipment.add("Trinket");
                c.equipment.add("Common clothes");
                c.skills.put(ATHLETICS, 1);
                c.skills.put(PERCEPTION, 1);

//        myLifestyle = 3;
                c.tools.add("Navigator's tools");
                c.tools.add("Water vehicles");
                c.gold += 10;
                break;
            case("Soldier"):
                c.personality=Util.pickMultiple(Const.soldierPersonality, 2);
                c.ideal=Util.pickOne(Const.soldierIdeal);
                c.flaw=Util.pickOne(Const.soldierFlaw);
                c.bond=Util.pickOne(Const.soldierBond);
                c.equipment.add("Insignia of rank");
                c.equipment.add("Common clothes");
                c.equipment.add("War trophy");
                c.equipment.add("Bone dice");
                c.tools.add("Land vehicles");
                c.skills.put(ATHLETICS, 1);
                c.skills.put(INTIMIDATION, 1);
                c.tools.add(Util.pickOne(Const.gaming));
//        myLifestyle = 3;

                c.gold += 10;
                x=Roll.d(8)-1;
                if (x==0)
                    c.background += " (Officer)";
                if (x==1)
                    c.background += " (Scout)";
                if (x==2)
                    c.background += " (Infantry)";
                if (x==3)
                    c.background += " (Cavalry)";
                if (x==4)
                    c.background += " (Healer)";
                if (x==5)
                    c.background += " (Quartermaster)";
                if (x==6)
                    c.background += " (Standard bearer)";
                if (x==7)
                    c.background += " (Support staff)";
                break;
            case("Urchin"):
                c.personality=Util.pickMultiple(Const.urchinPersonality, 2);
                c.ideal=Util.pickOne(Const.urchinIdeal);
                c.flaw=Util.pickOne(Const.urchinFlaw);
                c.bond=Util.pickOne(Const.urchinBond);
                c.equipment.add("Token");
                c.equipment.add("Common clothes");
                c.tools.add("Disguise kit");
                c.tools.add("Thieves' tools");
                c.tools.add("");
                c.skills.put(SLEIGHT_OF_HAND, 1);
                c.skills.put(STEALTH, 1);

//        myLifestyle = 3;
                c.gold += 10;
                break;
            default:
            System.out.println("no bg error!");
                break;

        }
    }


}
